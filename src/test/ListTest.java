package test;

import collections.ArrayList;
import collections.List;
import java.util.Random;

public class ListTest {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        System.out.println(list);
        System.out.println("list.isEmpty() = " + list.isEmpty());
        System.out.println("100 times of list.add(rand.nextInt(1000))");
        Random rand = new Random(47);
        for (int i=0; i<100; i++)
            list.add(rand.nextInt(1000));
        System.out.println(list);
        System.out.println("list.size() = " + list.size());
        System.out.println("---------------------------------------------");
        System.out.println("list.remove(0) = " + list.remove(99));
        System.out.println(list);
        System.out.println("list.size() = " + list.size());
        System.out.println("---------------------------------------------");
        System.out.println("list.remove((Integer)555) = " + list.remove((Integer)555));
        System.out.println(list);
        System.out.println("list.size() = " + list.size());
        System.out.println("----------------------------------------------");
        System.out.println("list.get(5) = " + list.get(5));
        System.out.println("list.contains(693) = " + list.contains(693));
        System.out.println("list.contains(18) = " + list.contains(18));
    }
}
