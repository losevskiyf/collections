package collections;

public class ArrayList<T> implements List<T>{

    private int capacity = 10;
    private Object[] array = new Object[capacity];
    private int size = 0;

    private void reSize(){
        capacity = capacity + capacity/2 + 1;
        Object[] newArray = new Object[capacity];
        for(int i=0; i<size; i++)
            newArray[i] = array[i];
        array = newArray;
    }

    @Override
    public boolean add(T element) {
        if(size==capacity)
            reSize();
        array[size] = element;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object element) {
        for (int i=0;i<size;i++) {
            if (element.equals(array[i])) {
                for(int j=i; j<size-1;j++){
                    array[j]=array[j+1];
                }
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public T remove(int index) {

        if(index <0 || index>=size)
            return null;

        T result = (T) array[index];
        for(int i=index; i<size-1; i++){
            array[i] = array[i+1];
        }
        size--;
        return result;

    }

    @Override
    public boolean contains(Object element) {
        for (int i=0;i<size;i++)
            if (element.equals(array[i])) return true;
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0 ? true : false;
    }

    @Override
    public T get(int index) {
        if(index<0 || index>=size) return null;
        return (T)array[index];
    }

    @Override
    public void clear() {
        for (int i=0; i<size; i++){
            array[i] = null;
        }
        size=0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i=0; i<size; i++)
            sb.append(array[i] + ",");
        if(size>0)sb.deleteCharAt(sb.length()-1);
        sb.append("]");
        return sb.toString();
    }
}
