package collections;

public interface List<T> {
    boolean add(T element);
    boolean remove(T element);
    T remove(int index);
    boolean contains(Object element);
    int size();
    boolean isEmpty();
    T get(int index);
    void clear();
}
